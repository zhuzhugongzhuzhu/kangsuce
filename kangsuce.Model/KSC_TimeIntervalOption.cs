﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace kangsuce.Model
{
    //KSC_TimeIntervalOption
    public class KSC_TimeIntervalOption
    {
        /// <summary>
        /// timeIntervalOptionId
        /// </summary>		
        private int _timeintervaloptionid;

        [Key]
        public int timeIntervalOptionId
        {
            get { return _timeintervaloptionid; }
            set { _timeintervaloptionid = value; }
        }

        /// <summary>
        /// timeIntervalId
        /// </summary>		
        private int _timeintervalid;

        public int timeIntervalId
        {
            get { return _timeintervalid; }
            set { _timeintervalid = value; }
        }

        /// <summary>
        /// 时段项名称
        /// </summary>		
        private string _timeintervaloptionname;

        public string timeIntervalOptionName
        {
            get { return _timeintervaloptionname; }
            set { _timeintervaloptionname = value; }
        }
    }
}