﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kangsuce.Model
{
    public class KSC_Video
    {
        private int _videoid;

        /// <summary>
        /// videoId
        /// </summary>		
        [Key]
        public int videoId
        {
            get { return _videoid; }
            set { _videoid = value; }
        }

        private string _videoname;

        /// <summary>
        /// 视频标题
        /// </summary>		
        public string videoName
        {
            get { return _videoname; }
            set { _videoname = value; }
        }

        private string _filepath;

        /// <summary>
        /// 视频文件路径
        /// </summary>		
        public string filePath
        {
            get { return _filepath; }
            set { _filepath = value; }
        }


        private string _imgpath;

        /// <summary>
        /// 封面图片
        /// </summary>		
        public string imgPath
        {
            get { return _imgpath; }
            set { _imgpath = value; }
        }

        private int? _videotime;

        /// <summary>
        /// 时间长度（单位秒）
        /// </summary>		
        public int? videoTime
        {
            get { return _videotime; }
            set { _videotime = value; }
        }
    }
}