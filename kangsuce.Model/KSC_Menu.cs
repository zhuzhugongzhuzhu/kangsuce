﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace kangsuce.Model
{
    /// <summary>
    /// 菜单表
    /// </summary>
    public class KSC_Menu
    {
        /// <summary>
        /// menuId
        /// </summary>		
        private int _menuid;

        [Key]
        public int menuId
        {
            get { return _menuid; }
            set { _menuid = value; }
        }

        /// <summary>
        /// 模块id
        /// </summary>		
        private int _functionid;

        public int functionId
        {
            get { return _functionid; }
            set { _functionid = value; }
        }

        /// <summary>
        /// 模块名称
        /// </summary>
        [NotMapped]
        public string functionName { get; set; }

        /// <summary>
        /// 父级Id
        /// </summary>		
        private int _parentid;

        public int parentId
        {
            get { return _parentid; }
            set { _parentid = value; }
        }

        /// <summary>
        /// 菜单名称
        /// </summary>		
        private string _menuname;

        public string menuName
        {
            get { return _menuname; }
            set { _menuname = value; }
        }

        /// <summary>
        /// 菜单地址
        /// </summary>		
        private string _menuurl;

        public string menuUrl
        {
            get { return _menuurl; }
            set { _menuurl = value; }
        }

        /// <summary>
        /// 排序
        /// </summary>		
        private int _menuorder;

        public int menuOrder
        {
            get { return _menuorder; }
            set { _menuorder = value; }
        }

        /// <summary>
        /// 是否可见
        /// </summary>		
        private string _menudisplay;

        public string menuDisplay
        {
            get { return _menudisplay; }
            set { _menudisplay = value; }
        }

        /// <summary>
        /// 菜单图标
        /// </summary>		
        private string _menuicon;

        public string menuIcon
        {
            get { return _menuicon; }
            set { _menuicon = value; }
        }
    }
}