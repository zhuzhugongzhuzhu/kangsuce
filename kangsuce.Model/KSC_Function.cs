﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace kangsuce.Model
{
    /// <summary>
    /// 模块表
    /// </summary>
    public class KSC_Function
    {
        /// <summary>
        /// functionId
        /// </summary>
        		
        private int _functionid;

        [Key]
        public int functionId
        {
            get { return _functionid; }
            set { _functionid = value; }
        }

        /// <summary>
        /// 模块名称
        /// </summary>		
        private string _functionname;

        public string functionName
        {
            get { return _functionname; }
            set { _functionname = value; }
        }

        /// <summary>
        /// 模块图标
        /// </summary>		
        private string _functionicon;

        public string functionIcon
        {
            get { return _functionicon; }
            set { _functionicon = value; }
        }

        /// <summary>
        /// 排序
        /// </summary>		
        private int _functionorder;

        public int functionOrder
        {
            get { return _functionorder; }
            set { _functionorder = value; }
        }

        /// <summary>
        /// functionDisplay
        /// </summary>		
        private string _functiondisplay;

        public string functionDisplay
        {
            get { return _functiondisplay; }
            set { _functiondisplay = value; }
        }

        /// <summary>
        /// 菜单集合
        /// </summary>		
        private List<KSC_Menu> _menuList;

        /// <summary>
        /// 菜单集合
        /// </summary>
        [NotMapped]
        public List<KSC_Menu> menuList
        {
            get { return _menuList; }
            set { _menuList = value; }
        }
    }
}