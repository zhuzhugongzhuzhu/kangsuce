﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace kangsuce.Model
{
    //检测项
    public class KSC_DetectionItem
    {
        /// <summary>
        /// detectionItemId
        /// </summary>		
        private int _detectionitemid;

        [Key]
        public int detectionItemId
        {
            get { return _detectionitemid; }
            set { _detectionitemid = value; }
        }

        /// <summary>
        /// 检测项分类Id
        /// </summary>		
        private int _detectionitemgroupid;

        public int detectionItemGroupId
        {
            get { return _detectionitemgroupid; }
            set { _detectionitemgroupid = value; }
        }

        /// <summary>
        /// 检测项名称
        /// </summary>		
        private string _detectionitemname;

        public string detectionItemName
        {
            get { return _detectionitemname; }
            set { _detectionitemname = value; }
        }

        /// <summary>
        /// 单位Id
        /// </summary>		
        private int _unitid;

        public int unitId
        {
            get { return _unitid; }
            set { _unitid = value; }
        }

        /// <summary>
        /// 时段Id
        /// </summary>		
        private int _timeintervalid;

        public int timeIntervalId
        {
            get { return _timeintervalid; }
            set { _timeintervalid = value; }
        }
    }
}