﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace kangsuce.Model
{
    /// <summary>
    /// 管理员表
    /// </summary>
    public class KSC_SysUser
    {
        /// <summary>
        /// userId
        /// </summary>		
        private int _userid;

        [Key]
        public int userId
        {
            get { return _userid; }
            set { _userid = value; }
        }


        /// <summary>
        /// 登录名
        /// </summary>		
        private string _loginName;

        public string loginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }

        /// <summary>
        /// 用户名
        /// </summary>		
        private string _username;

        public string userName
        {
            get { return _username; }
            set { _username = value; }
        }

        /// <summary>
        /// 密码
        /// </summary>		
        private string _password;

        public string passWord
        {
            get { return _password; }
            set { _password = value; }
        }

        /// <summary>
        /// 用户类别
        /// </summary>		
        private int _usertype;

        public int userType
        {
            get { return _usertype; }
            set { _usertype = value; }
        }

        /// <summary>
        /// createDate
        /// </summary>		
        private DateTime _createdate;

        public DateTime createDate
        {
            get { return _createdate; }
            set { _createdate = value; }
        }

        /// <summary>
        /// 最后登录时间
        /// </summary>		
        private DateTime? _lastlogindate;

        public DateTime? lastLoginDate
        {
            get { return _lastlogindate; }
            set { _lastlogindate = value; }
        }

        private string _isDisable;
        /// <summary>
        /// 是否禁用
        /// </summary>
        public string isDisable
        {
            get { return _isDisable; }
            set { _isDisable = value; }
        }

        /// <summary>
        /// 备用字段1
        /// </summary>		
        private string _attr1;

        public string attr1
        {
            get { return _attr1; }
            set { _attr1 = value; }
        }

        /// <summary>
        /// 备用字段2
        /// </summary>		
        private string _attr2;

        public string attr2
        {
            get { return _attr2; }
            set { _attr2 = value; }
        }


        /// <summary>
        /// 用户角色集合
        /// </summary>
        [NotMapped]
        public List<KSC_UserRole> userRoleList { get; set; }
    }
}