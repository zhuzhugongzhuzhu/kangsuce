﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace kangsuce.Model
{
    /// <summary>
    /// 微信用户表
    /// </summary>
    public class KSC_WxUser
    {
        /// <summary>
        /// userId
        /// </summary>		
        private int _userid;

        [Key]
        public int userId
        {
            get { return _userid; }
            set { _userid = value; }
        }

        /// <summary>
        /// 性别
        /// </summary>		
        private int _usersex;

        /// <summary>
        /// 性别 0：女 1：男
        /// </summary>	
        public int userSex
        {
            get { return _usersex; }
            set { _usersex = value; }
        }

        /// <summary>
        /// 出生日期
        /// </summary>		
        private DateTime _birthdate;

        /// <summary>
        /// 出生日期
        /// </summary>	
        public DateTime birthDate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }

        /// <summary>
        /// 手机号
        /// </summary>		
        private string _phone;

        /// <summary>
        /// 手机号
        /// </summary>	
        public string phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        /// <summary>
        /// 体重
        /// </summary>		
        private int? _weight;

        /// <summary>
        /// 体重
        /// </summary>		
        public int? weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        /// <summary>
        /// 身高
        /// </summary>		
        private int? _height;

        /// <summary>
        /// 身高
        /// </summary>		
        public int? height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// 特殊时期 0：无 1：备孕 2：怀孕 3：哺乳期 
        /// </summary>		
        private int? _specialperiod;

        /// <summary>
        /// 特殊时期 0：无 1：备孕 2：怀孕 3：哺乳期 
        /// </summary>	
        public int? specialPeriod
        {
            get { return _specialperiod; }
            set { _specialperiod = value; }
        }

        /// <summary>
        /// 直系亲属糖尿病史 0：无1：父母 2：祖辈 3：不知道 
        /// </summary>		
        private int? _diabeteshistory;

        /// <summary>
        /// 直系亲属糖尿病史 0：无1：父母 2：祖辈 3：不知道 
        /// </summary>	
        public int? diabetesHistory
        {
            get { return _diabeteshistory; }
            set { _diabeteshistory = value; }
        }

        /// <summary>
        /// 直系亲属高血压史 0：无1：父母 2：祖辈 3：不知道 
        /// </summary>		
        private int? _hypertensionhistory;

        /// <summary>
        /// 直系亲属高血压史 0：无1：父母 2：祖辈 3：不知道 
        /// </summary>		
        public int? hypertensionHistory
        {
            get { return _hypertensionhistory; }
            set { _hypertensionhistory = value; }
        }

        /// <summary>
        /// 直系亲属肿瘤患者 0：无1：父母 2：祖辈 3：不知道 
        /// </summary>		
        private int? _tumorpatients;

        /// <summary>
        /// 直系亲属肿瘤患者 0：无1：父母 2：祖辈 3：不知道 
        /// </summary>
        public int? tumorPatients
        {
            get { return _tumorpatients; }
            set { _tumorpatients = value; }
        }

        /// <summary>
        /// 吸烟状况 0：否 1：是 2：已戒烟 
        /// </summary>		
        private int? _smokestatus;

        /// <summary>
        /// 吸烟状况 0：否 1：是 2：已戒烟 
        /// </summary>
        public int? smokeStatus
        {
            get { return _smokestatus; }
            set { _smokestatus = value; }
        }

        /// <summary>
        /// 饮酒状况 0：从不 1：偶尔 2：经常 3：每天 
        /// </summary>		
        private int? _drinkstatus;

        /// <summary>
        /// 饮酒状况 0：从不 1：偶尔 2：经常 3：每天 
        /// </summary>	
        public int? drinkStatus
        {
            get { return _drinkstatus; }
            set { _drinkstatus = value; }
        }

        /// <summary>
        /// 饮食是否规律 0：否 1：是 
        /// </summary>		
        private int? _dietisrules;

        /// <summary>
        /// 饮食是否规律 0：否 1：是 
        /// </summary>	
        public int? dietIsRules
        {
            get { return _dietisrules; }
            set { _dietisrules = value; }
        }

        /// <summary>
        /// 睡眠是否规律 0：否 1：是 
        /// </summary>		
        private int? _sleepisrules;

        /// <summary>
        /// 睡眠是否规律 0：否 1：是 
        /// </summary>	
        public int? sleepIsRules
        {
            get { return _sleepisrules; }
            set { _sleepisrules = value; }
        }

        /// <summary>
        /// 运动状况 0：从不 1：偶尔 2：经常 3：每天 
        /// </summary>		
        private int? _motionstatus;

        /// <summary>
        /// 运动状况 0：从不 1：偶尔 2：经常 3：每天 
        /// </summary>		
        public int? motionStatus
        {
            get { return _motionstatus; }
            set { _motionstatus = value; }
        }

        /// <summary>
        /// 是否删除
        /// </summary>		
        private bool _isdelete;

        /// <summary>
        /// 是否删除
        /// </summary>	
        public bool isDelete
        {
            get { return _isdelete; }
            set { _isdelete = value; }
        }

        /// <summary>
        /// 微信openid
        /// </summary>		
        private string _openid;

        /// <summary>
        /// 微信openid
        /// </summary>	
        public string openId
        {
            get { return _openid; }
            set { _openid = value; }
        }

        /// <summary>
        /// 微信昵称
        /// </summary>		
        private string _nickname;

        /// <summary>
        /// 微信昵称
        /// </summary>		
        public string nickName
        {
            get { return _nickname; }
            set { _nickname = value; }
        }

        /// <summary>
        /// 微信头像
        /// </summary>		
        private string _headimgurl;

        /// <summary>
        /// 微信头像
        /// </summary>		
        public string headImgUrl
        {
            get { return _headimgurl; }
            set { _headimgurl = value; }
        }

        /// <summary>
        /// 创建日期
        /// </summary>		
        private DateTime _createdate;

        /// <summary>
        /// 创建日期
        /// </summary>		
        public DateTime createDate
        {
            get { return _createdate; }
            set { _createdate = value; }
        }

        /// <summary>
        /// 删除日期
        /// </summary>		
        private DateTime? _deletedate;

        /// <summary>
        /// 删除日期
        /// </summary>		
        public DateTime? deleteDate
        {
            get { return _deletedate; }
            set { _deletedate = value; }
        }

        /// <summary>
        /// 删除人
        /// </summary>		
        private int? _deleteuserid;

        /// <summary>
        /// 删除人
        /// </summary>
        public int? deleteUserId
        {
            get { return _deleteuserid; }
            set { _deleteuserid = value; }
        }

        /// <summary>
        /// 备用字段1
        /// </summary>		
        private string _attr1;

        public string attr1
        {
            get { return _attr1; }
            set { _attr1 = value; }
        }

        /// <summary>
        /// 备用字段2
        /// </summary>		
        private string _attr2;

        public string attr2
        {
            get { return _attr2; }
            set { _attr2 = value; }
        }

        /// <summary>
        /// 备用字段3
        /// </summary>		
        private string _attr3;

        public string attr3
        {
            get { return _attr3; }
            set { _attr3 = value; }
        }

        /// <summary>
        /// 备用字段4
        /// </summary>		
        private string _attr4;

        public string attr4
        {
            get { return _attr4; }
            set { _attr4 = value; }
        }

        /// <summary>
        /// 备用字段5
        /// </summary>		
        private string _attr5;

        public string attr5
        {
            get { return _attr5; }
            set { _attr5 = value; }
        }
    }
}