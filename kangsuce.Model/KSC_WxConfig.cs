﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kangsuce.Model
{
    public class KSC_WxConfig
    {
        public string AppId { set; get; }

        public string AppSecret { set; get; }

        public string Token { set; get; }

        public string EncodingAESKey { set; get; }
    }
}
