﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
namespace kangsuce.Model
{
    //KSC_WxMenu
    public class KSC_WxMenu
    {

        /// <summary>
        /// menuId
        /// </summary>		
        private int _menuid;
        [Key]
        public int menuId
        {
            get { return _menuid; }
            set { _menuid = value; }
        }
        /// <summary>
        /// 父Id
        /// </summary>		
        private int _pid;
        public int pId
        {
            get { return _pid; }
            set { _pid = value; }
        }
        /// <summary>
        /// 菜单名称
        /// </summary>		
        private string _menuname;
        public string menuName
        {
            get { return _menuname; }
            set { _menuname = value; }
        }
        /// <summary>
        /// 菜单类型
        /// </summary>		
        private string _menutype;
        public string menuType
        {
            get { return _menutype; }
            set { _menutype = value; }
        }
        /// <summary>
        /// 排序
        /// </summary>		
        private int _menuorder;
        public int menuOrder
        {
            get { return _menuorder; }
            set { _menuorder = value; }
        }
        /// <summary>
        /// 菜单关键字id
        /// </summary>		
        private int? _menukey;
        public int? menuKey
        {
            get { return _menukey; }
            set { _menukey = value; }
        }
        /// <summary>
        /// 菜单url
        /// </summary>		
        private string _menuurl;
        public string menuUrl
        {
            get { return _menuurl; }
            set { _menuurl = value; }
        }

        /// <summary>
        /// 子集集合
        /// </summary>
        [NotMapped]
        public List<KSC_WxMenu> chirdList { get; set; }

    }
}

