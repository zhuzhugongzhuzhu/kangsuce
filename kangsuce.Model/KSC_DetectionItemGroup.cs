﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace kangsuce.Model
{
    //KSC_DetectionItemGroup
    public class KSC_DetectionItemGroup
    {
        /// <summary>
        /// detectionItemGroupId
        /// </summary>		
        private int _detectionitemgroupid;

        [Key]
        public int detectionItemGroupId
        {
            get { return _detectionitemgroupid; }
            set { _detectionitemgroupid = value; }
        }

        /// <summary>
        /// 检测项分类名称
        /// </summary>		
        private string _detectionitemgroupname;

        public string detectionItemGroupName
        {
            get { return _detectionitemgroupname; }
            set { _detectionitemgroupname = value; }
        }

        /// <summary>
        /// 创建日期
        /// </summary>		
        private DateTime _createdate;

        public DateTime CreateDate
        {
            get { return _createdate; }
            set { _createdate = value; }
        }

        /// <summary>
        /// 创建人
        /// </summary>		
        private int _createuser;

        public int CreateUser
        {
            get { return _createuser; }
            set { _createuser = value; }
        }
    }
}