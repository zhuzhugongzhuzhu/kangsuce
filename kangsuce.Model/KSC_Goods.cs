﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kangsuce.Model
{
    public class KSC_Goods
    {
        /// <summary>
        /// goodsId
        /// </summary>		
        private int _goodsid;

        [Key]
        public int goodsId
        {
            get { return _goodsid; }
            set { _goodsid = value; }
        }

        /// <summary>
        /// 标题
        /// </summary>		
        private string _goodstitle;

        public string goodsTitle
        {
            get { return _goodstitle; }
            set { _goodstitle = value; }
        }

        /// <summary>
        /// 简介
        /// </summary>		
        private string _goodssummary;

        public string goodsSummary
        {
            get { return _goodssummary; }
            set { _goodssummary = value; }
        }

        /// <summary>
        /// 链接地址
        /// </summary>		
        private string _goodsurl;

        public string goodsUrl
        {
            get { return _goodsurl; }
            set { _goodsurl = value; }
        }

        /// <summary>
        /// 文件路径
        /// </summary>		
        private string _goodsfilepath;

        public string goodsFilePath
        {
            get { return _goodsfilepath; }
            set { _goodsfilepath = value; }
        }
    }
}