﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace kangsuce.Model
{
    //KSC_TimeInterval
    public class KSC_TimeInterval
    {
        /// <summary>
        /// timeIntervalId
        /// </summary>		
        private int _timeintervalid;

        [Key]
        public int timeIntervalId
        {
            get { return _timeintervalid; }
            set { _timeintervalid = value; }
        }

        /// <summary>
        /// 时段名
        /// </summary>		
        private string _timeintervalname;

        public string timeIntervalName
        {
            get { return _timeintervalname; }
            set { _timeintervalname = value; }
        }
    }
}