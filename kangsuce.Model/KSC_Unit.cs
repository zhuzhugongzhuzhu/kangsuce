﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace kangsuce.Model
{
    //KSC_Unit
    public class KSC_Unit
    {
        /// <summary>
        /// unitId
        /// </summary>		
        private int _unitid;

        [Key]
        public int unitId
        {
            get { return _unitid; }
            set { _unitid = value; }
        }

        /// <summary>
        /// 单位名称
        /// </summary>		
        private string _unitname;

        public string unitName
        {
            get { return _unitname; }
            set { _unitname = value; }
        }
    }
}