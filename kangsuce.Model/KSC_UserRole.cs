﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace kangsuce.Model
{
    //用户角色
    public class KSC_UserRole
    {
        /// <summary>
        /// userRoleId
        /// </summary>		
        private int _userroleid;

        [Key]
        public int userRoleId
        {
            get { return _userroleid; }
            set { _userroleid = value; }
        }

        /// <summary>
        /// 用户Id
        /// </summary>		
        private int _userid;

        public int userId
        {
            get { return _userid; }
            set { _userid = value; }
        }

        /// <summary>
        /// 角色Id
        /// </summary>		
        private int _roleid;

        public int roleId
        {
            get { return _roleid; }
            set { _roleid = value; }
        }

        /// <summary>
        /// 角色权限集合
        /// </summary>
        [NotMapped]
        public List<KSC_RolePermission> rolePermissionList { get; set; }
    }
}