﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace kangsuce.Model
{
    //KSC_DetectionResult
    public class KSC_DetectionResult
    {
        /// <summary>
        /// detectionResultId
        /// </summary>		
        private int _detectionresultid;

        [Key]
        public int detectionResultId
        {
            get { return _detectionresultid; }
            set { _detectionresultid = value; }
        }

        	
        private int _detectionItemId;

        /// <summary>
        /// 检测项id
        /// </summary>	
        public int detectionItemId
        {
            get { return _detectionItemId; }
            set { _detectionItemId = value; }
        }

        /// <summary>
        /// detectionTime
        /// </summary>		
        private DateTime _detectiontime;

        public DateTime detectionTime
        {
            get { return _detectiontime; }
            set { _detectiontime = value; }
        }

        /// <summary>
        /// resultValue
        /// </summary>		
        private decimal _resultvalue;

        public decimal resultValue
        {
            get { return _resultvalue; }
            set { _resultvalue = value; }
        }

        /// <summary>
        /// timeIntervalOptionId
        /// </summary>		
        private int _timeintervaloptionid;

        public int timeIntervalOptionId
        {
            get { return _timeintervaloptionid; }
            set { _timeintervaloptionid = value; }
        }

        /// <summary>
        /// remarks
        /// </summary>		
        private string _remarks;

        public string remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
    }
}