﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace kangsuce.Model
{
    /// <summary>
    /// 角色权限
    /// </summary>
    public class KSC_RolePermission
    {
        /// <summary>
        /// rolePermissionId
        /// </summary>		
        private int _rolepermissionid;

        [Key]
        public int rolePermissionId
        {
            get { return _rolepermissionid; }
            set { _rolepermissionid = value; }
        }

        /// <summary>
        /// 角色Id
        /// </summary>		
        private int _roleid;

        public int roleId
        {
            get { return _roleid; }
            set { _roleid = value; }
        }

        /// <summary>
        /// 模块Id
        /// </summary>		
        private int _functionid;

        public int functionId
        {
            get { return _functionid; }
            set { _functionid = value; }
        }

        /// <summary>
        /// 菜单Id
        /// </summary>		
        private int _menuid;

        public int menuId
        {
            get { return _menuid; }
            set { _menuid = value; }
        }
    }
}