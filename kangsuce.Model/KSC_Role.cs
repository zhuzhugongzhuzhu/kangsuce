﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace kangsuce.Model
{
    //KSC_Role
    public class KSC_Role
    {
        /// <summary>
        /// roleId
        /// </summary>		
        private int _roleid;

        [Key]
        public int roleId
        {
            get { return _roleid; }
            set { _roleid = value; }
        }

        /// <summary>
        /// 角色名称
        /// </summary>		
        private string _rolename;

        public string roleName
        {
            get { return _rolename; }
            set { _rolename = value; }
        }

        /// <summary>
        /// 创建人id
        /// </summary>		
        private int _createuserid;

        public int createUserId
        {
            get { return _createuserid; }
            set { _createuserid = value; }
        }

        /// <summary>
        /// 创建时间
        /// </summary>		
        private DateTime _createdate;

        public DateTime createDate
        {
            get { return _createdate; }
            set { _createdate = value; }
        }

        /// <summary>
        /// 是否可见 0：否 1：是
        /// </summary>		
        private string _roledisplay;

        public string roleDisplay
        {
            get { return _roledisplay; }
            set { _roledisplay = value; }
        }

        /// <summary>
        /// 菜单集合
        /// </summary>
        [NotMapped]
        public List<KSC_Menu> menuList { get; set; }
    }
}