﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kangsuce.Model;

namespace kangsuce.DAL
{
    public class MyContext : DbContext
    {
        /// <summary>
        /// 模块
        /// </summary>
        public DbSet<KSC_Function> KSC_Function { get; set; }

        /// <summary>
        /// 菜单
        /// </summary>
        public DbSet<KSC_Menu> KSC_Menu { get; set; }

        /// <summary>
        /// 系统用户
        /// </summary>
        public DbSet<KSC_SysUser> KSC_SysUser { get; set; }

        /// <summary>
        /// 微信用户
        /// </summary>
        public DbSet<KSC_WxUser> KSC_WxUser { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public DbSet<KSC_Role> KSC_Role { get; set; }

        /// <summary>
        /// 角色权限
        /// </summary>
        public DbSet<KSC_RolePermission> KSC_RolePermission { get; set; }

        /// <summary>
        /// 用户角色
        /// </summary>
        public DbSet<KSC_UserRole> KSC_UserRole { get; set; }

        /// <summary>
        /// 微信菜单
        /// </summary>
        public DbSet<KSC_WxMenu> KSC_WxMenu { get; set; }

        /// <summary>
        /// 联系购买
        /// </summary>
        public DbSet<KSC_Goods> KSC_Goods { get; set; }

        /// <summary>
        /// 检测项
        /// </summary>
        public DbSet<KSC_DetectionItem> KSC_DetectionItem { get; set; }

        /// <summary>
        /// 检测项分类
        /// </summary>
        public DbSet<KSC_DetectionItemGroup> KSC_DetectionItemGroup { get; set; }

        /// <summary>
        /// 检测项结果
        /// </summary>
        public DbSet<KSC_DetectionResult> KSC_DetectionResult { get; set; }

        /// <summary>
        /// 时段
        /// </summary>
        public DbSet<KSC_TimeInterval> KSC_TimeInterval { get; set; }

        /// <summary>
        /// 时段项
        /// </summary>
        public DbSet<KSC_TimeIntervalOption> KSC_TimeIntervalOption { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public DbSet<KSC_Unit> KSC_Unit { get; set; }

        /// <summary>
        /// 视频
        /// </summary>
        public DbSet<KSC_Video> KSC_Video { get; set; }

        public MyContext()
            : base("ConnString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}