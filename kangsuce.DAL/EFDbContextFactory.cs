﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace kangsuce.DAL
{
    public class EFDbContextFactory
    {
        public  MyContext GetCurrentDbContext()
        {
            //从CallContext数据槽中获取EF上下文
            MyContext objectContext = CallContext.GetData(typeof(EFDbContextFactory).FullName) as MyContext;
            if (objectContext == null)
            {
                //如果CallContext数据槽中没有EF上下文，则创建EF上下文，并保存到CallContext数据槽中
                objectContext = new MyContext();//当数据库替换为MySql等，只要在次出EF更换上下文即可。
                CallContext.SetData(typeof(EFDbContextFactory).FullName, objectContext);
            }
            return objectContext;
        }
    }
}
