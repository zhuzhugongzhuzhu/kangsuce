﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kangsuce.Model;

namespace kangsuce.DAL
{
    public class UnitOfWork : IDisposable
    {
        public MyContext context = new MyContext();
        private GenericRepository<KSC_Function> functionRepository;
        private GenericRepository<KSC_Menu> menuRepository;
        private GenericRepository<KSC_SysUser> sysUserRepository;
        private GenericRepository<KSC_WxUser> wxUserRepository;
        private GenericRepository<KSC_Role> roleRepository;
        private GenericRepository<KSC_RolePermission> rolePermissionRepository;
        private GenericRepository<KSC_UserRole> userRoleRepository;
        private GenericRepository<KSC_WxMenu> wxMenuRepository;
        private GenericRepository<KSC_Goods> goodsRepository;
        private GenericRepository<KSC_DetectionItem> detectionItemRepository;
        private GenericRepository<KSC_DetectionItemGroup> detectionItemGroupRepository;
        private GenericRepository<KSC_DetectionResult> detectionResultRepository;
        private GenericRepository<KSC_TimeInterval> timeIntervalRepository;
        private GenericRepository<KSC_TimeIntervalOption> timeIntervalOptionRepository;
        private GenericRepository<KSC_Unit> unitRepository;
        private GenericRepository<KSC_Video> videoRepository;


        /// <summary>
        /// 模块
        /// </summary>
        public GenericRepository<KSC_Function> FunctionRepository
        {
            get
            {
                if (this.functionRepository == null)
                {
                    this.functionRepository = new GenericRepository<KSC_Function>(context);
                }
                return functionRepository;
            }
        }

        /// <summary>
        /// 菜单
        /// </summary>
        public GenericRepository<KSC_Menu> MenuRepository
        {
            get
            {
                if (this.menuRepository == null)
                {
                    this.menuRepository = new GenericRepository<KSC_Menu>(context);
                }
                return menuRepository;
            }
        }

        /// <summary>
        /// 管理员
        /// </summary>
        public GenericRepository<KSC_SysUser> SysUserRepository
        {
            get
            {
                if (this.sysUserRepository == null)
                {
                    this.sysUserRepository = new GenericRepository<KSC_SysUser>(context);
                }
                return sysUserRepository;
            }
        }

        /// <summary>
        /// 微信用户
        /// </summary>
        public GenericRepository<KSC_WxUser> WxUserRepository
        {
            get
            {
                if (this.wxUserRepository == null)
                {
                    this.wxUserRepository = new GenericRepository<KSC_WxUser>(context);
                }
                return wxUserRepository;
            }
        }

        /// <summary>
        /// 角色
        /// </summary>
        public GenericRepository<KSC_Role> RoleRepository
        {
            get
            {
                if (this.roleRepository == null)
                {
                    this.roleRepository = new GenericRepository<KSC_Role>(context);
                }
                return roleRepository;
            }
        }

        /// <summary>
        /// 角色权限
        /// </summary>
        public GenericRepository<KSC_RolePermission> RolePermissionRepository
        {
            get
            {
                if (this.rolePermissionRepository == null)
                {
                    this.rolePermissionRepository = new GenericRepository<KSC_RolePermission>(context);
                }
                return rolePermissionRepository;
            }
        }

        /// <summary>
        /// 用户角色
        /// </summary>
        public GenericRepository<KSC_UserRole> UserRoleRepository
        {
            get
            {
                if (this.userRoleRepository == null)
                {
                    this.userRoleRepository = new GenericRepository<KSC_UserRole>(context);
                }
                return userRoleRepository;
            }
        }

        /// <summary>
        /// 微信菜单
        /// </summary>
        public GenericRepository<KSC_WxMenu> WxMenuRepository
        {
            get
            {
                if (this.wxMenuRepository == null)
                {
                    this.wxMenuRepository = new GenericRepository<KSC_WxMenu>(context);
                }
                return wxMenuRepository;
            }
        }

        /// <summary>
        /// 商品
        /// </summary>
        public GenericRepository<KSC_Goods> GoodsRepository
        {
            get
            {
                if (this.goodsRepository == null)
                {
                    this.goodsRepository = new GenericRepository<KSC_Goods>(context);
                }
                return goodsRepository;
            }
        }

        /// <summary>
        /// 检测项
        /// </summary>
        public GenericRepository<KSC_DetectionItem> DetectionItemRepository
        {
            get
            {
                if (this.detectionItemRepository == null)
                {
                    this.detectionItemRepository = new GenericRepository<KSC_DetectionItem>(context);
                }
                return detectionItemRepository;
            }
        }

        /// <summary>
        /// 检测项分类
        /// </summary>
        public GenericRepository<KSC_DetectionItemGroup> DetectionItemGroupRepository
        {
            get
            {
                if (this.detectionItemGroupRepository == null)
                {
                    this.detectionItemGroupRepository = new GenericRepository<KSC_DetectionItemGroup>(context);
                }
                return detectionItemGroupRepository;
            }
        }

        /// <summary>
        /// 检测项结果
        /// </summary>
        public GenericRepository<KSC_DetectionResult> DetectionResultRepository
        {
            get
            {
                if (this.detectionResultRepository == null)
                {
                    this.detectionResultRepository = new GenericRepository<KSC_DetectionResult>(context);
                }
                return detectionResultRepository;
            }
        }

        /// <summary>
        /// 检测项结果
        /// </summary>
        public GenericRepository<KSC_TimeInterval> TimeIntervalRepository
        {
            get
            {
                if (this.timeIntervalRepository == null)
                {
                    this.timeIntervalRepository = new GenericRepository<KSC_TimeInterval>(context);
                }
                return timeIntervalRepository;
            }
        }

        /// <summary>
        /// 时段项
        /// </summary>
        public GenericRepository<KSC_TimeIntervalOption> TimeIntervalOptionRepository
        {
            get
            {
                if (this.timeIntervalOptionRepository == null)
                {
                    this.timeIntervalOptionRepository = new GenericRepository<KSC_TimeIntervalOption>(context);
                }
                return timeIntervalOptionRepository;
            }
        }

        /// <summary>
        /// 单位
        /// </summary>
        public GenericRepository<KSC_Unit> UnitRepository
        {
            get
            {
                if (this.unitRepository == null)
                {
                    this.unitRepository = new GenericRepository<KSC_Unit>(context);
                }
                return unitRepository;
            }
        }

        /// <summary>
        /// 视频
        /// </summary>
        public GenericRepository<KSC_Video> VideoRepository
        {
            get
            {
                if (this.videoRepository == null)
                {
                    this.videoRepository = new GenericRepository<KSC_Video>(context);
                }
                return videoRepository;
            }
        }


        
        #region 执行sql语句

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="dbParameters">参数</param>
        /// <returns>返回影响条数</returns>
        public int ExecuteSqlCommand(string sql, params object[] dbParameters)
        {
            return context.Database.ExecuteSqlCommand(sql, dbParameters);
        }

        #region 返回datatable  

        public DataTable SqlQueryForDataTatable(string sql)
        {
            SqlConnection conn = new System.Data.SqlClient.SqlConnection
            {
                ConnectionString = context.Database.Connection.ConnectionString
            };
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();
            adapter.Fill(table);

            conn.Close(); //连接需要关闭  
            conn.Dispose();
            return table;
        }

        public DataTable SqlQueryForDataTatable(string sql, DbParameter[] parameters)
        {
            SqlConnection conn = new System.Data.SqlClient.SqlConnection
            {
                ConnectionString = context.Database.Connection.ConnectionString
            };
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            SqlCommand cmd = new SqlCommand
            {
                Connection = conn,
                CommandText = sql
            };

            if (parameters != null && parameters.Length > 0)
            {
                foreach (var item in parameters)
                {
                    cmd.Parameters.Add(item);
                }
            }
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();
            adapter.Fill(table);
            return table;
        }

        #endregion

        #endregion

        #region 提交数据库

        /// <summary>
        /// 保存
        /// </summary>
        public int Save()
        {
            return context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}