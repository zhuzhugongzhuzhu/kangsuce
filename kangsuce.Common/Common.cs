﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using kangsuce.Model;

namespace kangsuce.Common
{
    public class Common
    {
        /// <summary>
        /// 根据节点名称获取微信配置信息
        /// </summary>
        /// <returns></returns>
        public static KSC_WxConfig GetWxConfigText(string filePath)
        {
            try
            {
                XmlDocument xmldocument = new XmlDocument();
                xmldocument.Load(filePath);
                XmlSerializer formatter = new XmlSerializer(typeof(KSC_WxConfig));
                StreamReader sr = new StreamReader(filePath);
                KSC_WxConfig model = (KSC_WxConfig)formatter.Deserialize(sr);
                sr.Close();
                return model;
            }
            catch (Exception ex)
            {
                return new KSC_WxConfig();
            }
        }

        /// <summary>
        /// 根据节点名称获取微信配置信息
        /// </summary>
        /// <param name="nodes">节点集合（节点名称，节点值）</param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool UpdateWxConfigText(Dictionary<string, string> nodes,string filePath)
        {
            try
            {
                XmlDocument xmldocument = new XmlDocument();
                xmldocument.Load(filePath);
                foreach (KeyValuePair<string, string> item in nodes)
                {
                    XmlNode xmlnode = xmldocument.SelectSingleNode("/KSC_WxConfig/" + item.Key);
                    if (xmlnode != null)
                    {
                        xmlnode.InnerText = item.Value;
                    }
                }
                xmldocument.Save(filePath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
