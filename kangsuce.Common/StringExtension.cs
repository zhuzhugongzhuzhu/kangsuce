﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace kangsuce.Common
{
    /// <summary>
    /// String扩展类
    /// </summary>
    public static class StringExtension
    {
        #region 字符串转换为Pascal格式

        /// <summary>
        /// 字符串转换为Pascal格式
        /// </summary>
        /// <param name="s">要转换的字符串</param>
        /// <returns>返回Pascal格式字符串</returns>
        /// <example>输入myString,返回MyString这种字符串</example>
        public static string ToPascal(this string p_str)
        {
            return p_str.Substring(0, 1).ToUpper() + p_str.Substring(1);
        }

        #endregion

        #region 字符串转换为camel格式

        /// <summary>
        /// 字符串转换为camel格式
        /// </summary>
        /// <param name="p_str">要转换的字符串</param>
        /// <returns></returns>
        public static string ToCamel(this string p_str)
        {
            return p_str.Substring(0, 1).ToLower() + p_str.Substring(1);
        }

        #endregion

        #region 验证字符串是否是整数

        /// <summary>
        /// 验证字符串是否是整数
        /// </summary>
        /// <param name="s">要进行验证的字符串</param>
        /// <returns>整数：true  非整数:false</returns>
        public static bool IsInt(this string s)
        {
            try
            {
                Convert.ToInt32(s);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region 验证是否数字

        /// <summary>
        /// 验证是否数字
        /// </summary>
        /// <param name="str">要验证的字符串</param>
        /// <returns>是否符合 true 或者 false</returns>
        public static bool IsNumeric(this string str)
        {
            return MachRegex(@"^[-]?\d+[.]?\d*$", str);
        }

        #endregion

        #region 字符串转换为数字

        /// <summary>
        /// 字符串转换为 Int32格式
        /// </summary>
        /// <param name="p_self"></param>
        /// <param name="p_defaultValue">默认值(默认为0)</param>
        /// <returns>int类型字符串，出错返回int.MinValue</returns>
        public static int ToInt32(this object p_self, int p_defaultValue = 0)
        {
            try
            {
                return Convert.ToInt32(p_self);
            }
            catch
            {
                return p_defaultValue;
            }
        }

        /// <summary>
        /// 字符串转换为 Int64格式
        /// </summary>
        /// <param name="p_Self"></param>
        /// <returns>long类型字符串，出错返回int.MinValue</returns>
        public static long ToInt64(this object p_Self)
        {
            try
            {
                return Convert.ToInt64(p_Self);
            }
            catch
            {
                return int.MinValue;
            }
        }

        public static long ToInt64(this object self, long p_defaultValue)
        {
            try
            {
                return Convert.ToInt64(self);
            }
            catch
            {
                return p_defaultValue;
            }
        }

        #endregion

        #region  字符串是否为日期

        /// <summary>
        /// 字符串是否为日期
        /// </summary>
        /// <param name="s">要进行判断的字符串</param>
        /// <returns>是日期格式:true 不是：false</returns>
        public static bool IsDateTime(this string s)
        {
            try
            {
                Convert.ToDateTime(s);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region 字符串转换为日期

        /// <summary>
        ///     转换成日期
        /// </summary>
        /// <param name="obj">object</param>
        /// <param name="flag">当日期为空或错误时，0:返回最小时间；1：返回最大时间；2：返回当前时间</param>
        /// <returns></returns>
        public static DateTime ToDateTime(this object obj, int flag = 0)
        {
            DateTime date;
            if (obj == null || !DateTime.TryParse(obj.ToString(), out date))
            {
                switch (flag)
                {
                    case 0:
                        date = DateTime.MinValue;
                        break;
                    case 1:
                        date = DateTime.MaxValue;
                        break;
                    default:
                        date = DateTime.Now;
                        break;
                }
            }
            return date;
        }

        #endregion

        #region 转换成UTF-8字符串

        /// <summary>
        /// 转换成UTF-8字符串
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static String toUtf8String(this string s)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                char c = s[i];
                if (c >= 0 && c <= 255)
                {
                    sb.Append(c);
                }
                else
                {
                    byte[] b;
                    try
                    {
                        b = Encoding.UTF8.GetBytes(c.ToString());
                    }
                    catch (Exception ex)
                    {
                        b = new byte[0];
                    }
                    for (int j = 0; j < b.Length; j++)
                    {
                        int k = b[j];
                        if (k < 0) k += 256;

                        sb.Append("%" + Convert.ToString(k, 16).ToUpper());
                    }
                }
            }
            return sb.ToString();
        }

        #endregion

        #region 字符串编码转换

        /// <summary>
        /// 字符串编码转换
        /// </summary>
        /// <param name="str"></param>
        /// <param name="oldCoding">原编码</param>
        /// <param name="newEncoding">新编码</param>
        /// <returns></returns>
        public static string ConvertEnCoding(this string str, Encoding oldCoding, Encoding newEncoding)
        {
            return newEncoding.GetString(oldCoding.GetBytes(str));
        }

        #endregion

        #region IP地址转换为秘密的IP地址

        /// <summary>
        /// IP地址转换为秘密的IP地址
        /// </summary>
        /// <param name="p_ipAddress">如：202.195.224.100</param>
        /// <returns>返回202.195.224.*类型的字符串</returns>
        public static string ipSecret(this string p_ipAddress)
        {
            string[] ips = p_ipAddress.Split('.');
            StringBuilder sb_screcedIp = new StringBuilder();
            int ipsLength = ips.Length;
            for (int i = 0; i < ipsLength - 1; i++)
            {
                sb_screcedIp.Append(ips[i].ToString() + ".");
            }
            sb_screcedIp.Append("*");
            return sb_screcedIp.ToString();
        }

        #endregion

        #region base64url编码处理

        public static string ToEnBase64(this string str)
        {
            return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(str));
        }

        public static string ToUnBase64(this string str)
        {
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }

        #endregion

        #region 判断字符串是否在字符串数组中

        /// <summary>
        /// 判断字符串是否在字符串数组中
        /// </summary>
        /// <param name="str">要判断的字符串</param>
        /// <param name="p_targrt">目标数组</param>
        /// <returns></returns>
        public static bool IsInArray(this string str, string[] p_targrt)
        {
            return p_targrt.Contains(str);
            //for (int i = 0; i < targrt.Length; i++)
            //{
            //    if (str == targrt[i])
            //    {
            //        return true;
            //    }
            //}
            //return false;
        }

        #endregion

        #region 字符串数组转换为数字数组

        /// <summary>
        /// 字符串数组转换为数字数组
        /// </summary>
        /// <param name="p_stringArray"></param>
        /// <returns></returns>
        public static int[] toIntArray(this string[] p_stringArray)
        {
            int[] returnValue = new int[p_stringArray.Length];
            for (int i = 0; i < p_stringArray.Length; i++)
            {
                returnValue[i] = Convert.ToInt32(p_stringArray[i]);
            }
            return returnValue;
        }

        #endregion

        #region 去除HTML标记

        /// <summary>
        /// 去除HTML标记
        /// </summary>
        /// <param name="p_strHtml">要除去HTML标记的文本</param>
        /// <returns></returns>
        public static string TrimHTML(this string p_strHtml)
        {
            string StrNohtml = p_strHtml;
            StrNohtml = StrNohtml.TrimScript();
            StrNohtml = Regex.Replace(StrNohtml, "&.{1,6};", "", RegexOptions.IgnoreCase);

            StrNohtml = Regex.Replace(StrNohtml, "<p.*?>", Environment.NewLine, RegexOptions.IgnoreCase);
            StrNohtml = Regex.Replace(StrNohtml, "<br.*?>", Environment.NewLine, RegexOptions.IgnoreCase);

            StrNohtml = Regex.Replace(StrNohtml, "<script[\\w\\W].*?</script>", "", RegexOptions.IgnoreCase);
            StrNohtml = System.Text.RegularExpressions.Regex.Replace(StrNohtml, "<[^>]+>", "");
            StrNohtml = System.Text.RegularExpressions.Regex.Replace(StrNohtml, "&[^;]+;", "");

            return StrNohtml;
        }


        public static string TrimScript(this string str)
        {
            str = Regex.Replace(str, "<script[\\w\\W]*?</script>", "", RegexOptions.IgnoreCase);
            str = Regex.Replace(str, "<style[\\w\\W]*?</style>", "", RegexOptions.IgnoreCase);
            str = Regex.Replace(str, "<iframe[\\w\\W]*?</iframe>", "", RegexOptions.IgnoreCase);
            return str;
        }

        #endregion

        #region 去除换行符

        /// <summary>
        /// 去除换行符
        /// </summary>
        /// <param name="str">要进行处理的字符串</param>
        /// <returns></returns>
        public static string TrimBR(this string str)
        {
            str = str.Replace("\n", "");
            str = str.Replace("\r", "");
            str = str.Replace("\t", "");
            return str;
        }

        #endregion

        #region 截取文字

        /// <summary>
        /// 截取一段文字.
        /// </summary>
        /// <param name="str">要截取的原文本.</param>
        /// <param name="p_maxLength">截取长度，多少个字</param>
        /// <param name="suffix">The suffix.</param>
        /// <returns></returns>
        public static string Truncate(this string str, int p_maxLength, string suffix = "...")
        {
            // 剩余的用 ...替换
            var truncatedString = str;

            if (p_maxLength <= 0) return truncatedString;
            var strLength = p_maxLength - suffix.Length;

            if (strLength <= 0) return truncatedString;

            if (str == null || str.Length <= p_maxLength) return truncatedString;

            truncatedString = str.Substring(0, strLength);
            truncatedString = truncatedString.TrimEnd();
            truncatedString += suffix;

            return truncatedString;
        }

        #endregion

        #region 去除XSS攻击字符

        /// <summary>
        /// 清除字符串中带有的XSS攻击威胁的字符
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string CleanForXss(this string input)
        {
            //remove any html
            input = input.StripHtml();
            //strip out any potential chars involved with XSS
            return input.ExceptChars(new HashSet<char>("*?(){}[];:%<>/\\|&'\"".ToCharArray()));
        }

        public static string ExceptChars(this string str, HashSet<char> toExclude)
        {
            var sb = new StringBuilder(str.Length);
            foreach (var c in str.Where(c => toExclude.Contains(c) == false))
            {
                sb.Append(c);
            }
            return sb.ToString();
        }

        #endregion

        #region 去除全部HTML标签

        /// <summary>
        /// 去除全部HTML标签
        /// </summary>
        /// <param name="str">原始字符串</param>
        /// <returns>返回无任何标签的字符串</returns>
        public static string StripHtml(this string str)
        {
            const string const_pattern = @"<(.|\n)*?>";
            return Regex.Replace(str, const_pattern, String.Empty);
        }

        #endregion

        #region 删除字符串尾部的回车/换行/空格

        /// <summary>
        /// 删除字符串尾部的回车/换行/空格
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RTrim(this string str)
        {
            for (int i = str.Length; i >= 0; i--)
            {
                if (str[i].Equals(" ") || str[i].Equals("\r") || str[i].Equals("\n"))
                {
                    str.Remove(i, 1);
                }
            }
            return str;
        }

        #endregion

        #region SQL注入敏感字符剔除

        /// <summary>
        /// SQL注入敏感字符剔除
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSqlEnCode(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            System.Text.RegularExpressions.Regex regex1 =
                new System.Text.RegularExpressions.Regex(@"<script[\s\S]+</script *>",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex2 =
                new System.Text.RegularExpressions.Regex(@" href *= *[\s\S]*script *:",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex3 = new System.Text.RegularExpressions.Regex(@" on[\s\S]*=",
                System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex4 =
                new System.Text.RegularExpressions.Regex(@"<iframe[\s\S]+</iframe *>",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex5 =
                new System.Text.RegularExpressions.Regex(@"<frameset[\s\S]+</frameset *>",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex6 =
                new System.Text.RegularExpressions.Regex(
                    @"(\bselect\b.+\bfrom\b)|(\binsert\b.+\binto\b)|(\bdelete\b.+\bfrom\b)|(\bcount\b\(.+\))|(\bdrop\b\s+\btable\b)|(\bupdate\b.+\bset\b)|(\btruncate\b\s+\btable\b)|(\bxp_cmdshell\b)|(\bexec\b)|(\bexecute\b)|(\bnet\b\s+\blocalgroup\b)|(\bnet\b\s+\buser\b)",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            str = str.Replace("'", "''");
            str = regex1.Replace(str, ""); //过滤<script></script>标记 
            str = regex2.Replace(str, ""); //过滤href=javascript: (<A>) 属性 
            str = regex3.Replace(str, " _disibledevent="); //过滤其它控件的on...事件 
            str = regex4.Replace(str, ""); //过滤iframe 
            str = regex5.Replace(str, ""); //过滤frameset
            str = regex6.Replace(str, ""); //过滤frameset
            return str;
        }

        #endregion

        #region 生成以日期时间为基础的随机字符串

        /// <summary>
        /// 生成以日期时间为基础的随机字符串
        /// </summary>
        /// <returns></returns>
        public static string Getfilename()
        {
            Random number = new Random();
            //下面的number.Next(10000,99999).ToString()加入一个5位的在10000到99999之间的随机数
            //yyyyMMdd代码年月日。hhmmss代表小时分钟秒钟 。fff代表毫秒

            //暂时使用了GUID的那个文件名filenameGUID
            return DateTime.Now.ToString("yyyyMMddhhmmssfff") + "_" + number.Next(10000, 99999).ToString();
        }

        #endregion

        #region 返回GUID唯一标示符

        public static string GetGuid()
        {
            return Guid.NewGuid().ToString();
        }

        #endregion

        #region ///是否是GUID格式

        public static bool ISGUID(this string str)
        {
            return Regex.IsMatch(str,
                @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
        }

        #endregion

        #region --剔除脚本注入代码 已合并到上面的ToSqlEnCode来实现,暂时保留.

        ///// <summary>
        ///// 剔除脚本注入代码
        ///// </summary>
        ///// <param name="str"></param>
        ///// <returns></returns>
        //public static string TrimDbDangerousChar(this string str)
        //{

        //    return ToSqlEnCode(string str);
        //if (string.IsNullOrEmpty(str))
        // {
        //     return "";
        // }

        //    System.Text.RegularExpressions.Regex regex1 = new System.Text.RegularExpressions.Regex(@"<script[\s\S]+</script *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //    System.Text.RegularExpressions.Regex regex2 = new System.Text.RegularExpressions.Regex(@" href *= *[\s\S]*script *:", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //    System.Text.RegularExpressions.Regex regex3 = new System.Text.RegularExpressions.Regex(@" on[\s\S]*=", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //    System.Text.RegularExpressions.Regex regex4 = new System.Text.RegularExpressions.Regex(@"<iframe[\s\S]+</iframe *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //    System.Text.RegularExpressions.Regex regex5 = new System.Text.RegularExpressions.Regex(@"<frameset[\s\S]+</frameset *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //    str = str.Replace("'", "''");
        //    str = regex1.Replace(str, ""); //过滤<script></script>标记 
        //    str = regex2.Replace(str, ""); //过滤href=javascript: (<A>) 属性 
        //    str = regex3.Replace(str, " _disibledevent="); //过滤其它控件的on...事件 
        //    str = regex4.Replace(str, ""); //过滤iframe 
        //    str = regex5.Replace(str, ""); //过滤frameset
        //return str;
        // }

        #endregion

        #region  验证字符串是否为空字符串

        /// <summary>
        /// 验证字符串是否为空字符串
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        #endregion

        #region 检测是否有Sql危险字符

        /// <summary>
        /// 检测是否有Sql危险字符
        /// </summary>
        /// <param name="str">要判断字符串</param>
        /// <returns>判断结果</returns>
        public static bool IsSafeSqlString(this string str)
        {
            return !Regex.IsMatch(str, @"[-|;|,|\/|\(|\)|\[|\]|\}|\{|%|@|\*|!|\']");
        }

        #endregion

        #region 检测是否符合邮箱地址规则

        /// <summary>
        /// 检测是否是邮箱地址格式
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsanEmailString(this string str)
        {
            return Regex.IsMatch(str,
                @"^([a-zA-Z0-9]+[_|\-|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\-|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,6}$");
        }

        #endregion

        #region 验证字符串是否符合正则表达式MachRegex

        /// <summary>
        /// 验证字符串是否符合正则表达式MachRegex
        /// </summary>
        /// <param name="regex">正则表达式</param>
        /// <param name="str">字符串</param>
        /// <returns>是否符合 true 或者 false</returns>
        private static bool MachRegex(string regex, string str)
        {
            Regex reg = new Regex(regex);
            return reg.IsMatch(str);
        }

        #endregion
    }
}
