﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class MenuBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_Menu model)
        {
            unitOfWork.MenuRepository.Insert(model);
            unitOfWork.Save();
            return model.menuId;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.MenuRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_Menu model)
        {
            unitOfWork.MenuRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_Menu GetModelById(int id)
        {
            return unitOfWork.MenuRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_Menu GetModel(Expression<Func<KSC_Menu, bool>> filter)
        {
            return unitOfWork.MenuRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_Menu> GetList(Expression<Func<KSC_Menu, bool>> filter, bool isEdit = false)
        {
            List<KSC_Menu> list = unitOfWork.MenuRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_Menu> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_Menu, bool>> filter = null,
            Func<IQueryable<KSC_Menu>, IOrderedQueryable<KSC_Menu>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.MenuRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="sqlWhere">条件</param>
        /// <returns></returns>
        public List<KSC_Menu> GetListForFunction(int pageSize, int pageIndex, out int total,string sqlWhere=" 1=1 ")
        {
            string sql = "select m.*,f.functionName from KSC_Menu m left join KSC_Function f on m.functionId=f.functionId where  " + sqlWhere+ " order by f.functionOrder,menuOrder";
            DataTable dt = unitOfWork.SqlQueryForDataTatable(sql);
            List<KSC_Menu> list = (from DataRow dr in dt.Rows
                select new KSC_Menu
                {
                    functionId = Convert.ToInt32(dr["functionId"]),
                    functionName = dr["functionName"].ToString(),
                    menuDisplay = dr["menuDisplay"].ToString(),
                    menuIcon = dr["menuIcon"].ToString(),
                    menuId = Convert.ToInt32(dr["menuId"]),
                    menuName = dr["menuName"].ToString(),
                    menuOrder = Convert.ToInt32(dr["menuOrder"]),
                    menuUrl = dr["menuUrl"].ToString(),
                    parentId = Convert.ToInt32(dr["parentId"])
                }).ToList();
            total = list.Count;
            list = list.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            return list;
        }
    }
}