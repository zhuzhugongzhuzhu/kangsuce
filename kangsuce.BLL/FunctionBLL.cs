﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class FunctionBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_Function model)
        {
            unitOfWork.FunctionRepository.Insert(model);
            unitOfWork.Save();
            return model.functionId;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.FunctionRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_Function model)
        {
            unitOfWork.FunctionRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 查询实体
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="menuDisplay">是否显示隐藏菜单</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_Function> GetList(Expression<Func<KSC_Function, bool>> filter, string menuDisplay="1", bool isEdit = false)
        {
            List<KSC_Function> list = unitOfWork.FunctionRepository.GetList(filter, isEdit);
            foreach (KSC_Function model in list)
            {
                model.menuList =
                    unitOfWork.MenuRepository.GetList(
                        p => p.functionId == model.functionId && (menuDisplay == "" || p.menuDisplay == menuDisplay));
            }
            return list;
        }

        public IEnumerable<object> GetMenuList(Expression<Func<KSC_Menu, bool>> filter, int roleId = 0)
        {
            List<KSC_Function> flist = unitOfWork.FunctionRepository.GetList(p => p.functionDisplay == "1");
            List<KSC_Menu> mlist = unitOfWork.MenuRepository.GetList(filter).OrderBy(p => p.functionId).ToList();
            List<KSC_RolePermission> rplist =
                unitOfWork.RolePermissionRepository.GetList(p => roleId == 0 || p.roleId == roleId);
            var list =
                mlist.GroupJoin(rplist, m => m.menuId, rp => rp.menuId, (m, rp) => new {m, rp}).DefaultIfEmpty()
                    .Select(o => new
                    {
                        o.m.functionId,
                        o.m.menuId,
                        o.m.menuName,
                        isChecked = o.rp.GetType().Name == "Grouping" ? 1 : 0
                    });
            var list1 = list.Join(flist, m => m.functionId, f => f.functionId, (m, f) => new {m, f}).Select(o => new
            {
                o.f.functionId,
                o.f.functionIcon,
                o.f.functionName,
                o.f.functionOrder,
                o.m.isChecked,
                o.m.menuId,
                o.m.menuName
            });
            return list1;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_Function> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_Function, bool>> filter = null,
            Func<IQueryable<KSC_Function>, IOrderedQueryable<KSC_Function>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.FunctionRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}