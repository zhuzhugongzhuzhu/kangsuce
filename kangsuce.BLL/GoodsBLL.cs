﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Transactions;
using kangsuce.DAL;
using kangsuce.Model;
using kangsuce.Common;

namespace kangsuce.BLL
{
    public class GoodsBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_Goods model)
        {
            unitOfWork.GoodsRepository.Insert(model);
            unitOfWork.Save();
            return model.goodsId;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.GoodsRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool Delete(Expression<Func<KSC_Goods, bool>> filter)
        {
            unitOfWork.GoodsRepository.DeleteByFilter(filter);
            return unitOfWork.Save() > 0;
        }

        public bool Delete(List<int> idList, string path)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    List<KSC_Goods> list = GetList(p => idList.Contains(p.goodsId));
                    foreach (KSC_Goods model in list)
                    {
                        unitOfWork.GoodsRepository.Delete(model.goodsId);
                    }
                    unitOfWork.Save();
                    scope.Complete();
                    foreach (
                        string localPath in
                            list.Select(model => Path.Combine(path, model.goodsFilePath.Replace("/Upload/", ""))))
                    {
                        System.IO.File.Delete(localPath);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    return false;
                }
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_Goods model)
        {
            unitOfWork.GoodsRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_Goods GetModelById(int id)
        {
            return unitOfWork.GoodsRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_Goods GetModel(Expression<Func<KSC_Goods, bool>> filter)
        {
            return unitOfWork.GoodsRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <returns></returns>
        public List<KSC_Goods> GetList(Expression<Func<KSC_Goods, bool>> filter, bool isEdit = false,
            Func<IQueryable<KSC_Goods>, IOrderedQueryable<KSC_Goods>> orderBy = null)
        {
            List<KSC_Goods> list = unitOfWork.GoodsRepository.GetList(filter, isEdit, orderBy);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_Goods> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_Goods, bool>> filter = null,
            Func<IQueryable<KSC_Goods>, IOrderedQueryable<KSC_Goods>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.GoodsRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}