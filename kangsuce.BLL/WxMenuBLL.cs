﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class WxMenuBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_WxMenu model)
        {
            unitOfWork.WxMenuRepository.Insert(model);
            unitOfWork.Save();
            return model.menuId;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.WxMenuRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_WxMenu model)
        {
            unitOfWork.WxMenuRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_WxMenu GetModelById(int id)
        {
            return unitOfWork.WxMenuRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_WxMenu GetModel(Expression<Func<KSC_WxMenu, bool>> filter)
        {
            return unitOfWork.WxMenuRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="selectChird">是否查询子集</param>
        /// <returns></returns>
        public List<KSC_WxMenu> GetList(Expression<Func<KSC_WxMenu, bool>> filter, bool isEdit = false, Func<IQueryable<KSC_WxMenu>, IOrderedQueryable<KSC_WxMenu>> orderBy = null,bool selectChird=false)
        {
            List<KSC_WxMenu> list = unitOfWork.WxMenuRepository.GetList(filter, isEdit, orderBy);
            if (selectChird)
            {
                foreach (KSC_WxMenu item in list)
                {
                    item.chirdList = GetList(p => p.pId == item.menuId, false, p => p.OrderBy(o => o.menuOrder), false);
                }
            }
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_WxMenu> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_WxMenu, bool>> filter = null,
            Func<IQueryable<KSC_WxMenu>, IOrderedQueryable<KSC_WxMenu>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.WxMenuRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }

    }
}