﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class DetectionResultBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_DetectionResult model)
        {
            unitOfWork.DetectionResultRepository.Insert(model);
            unitOfWork.Save();
            return model.detectionResultId;
        }

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool Insert(List<KSC_DetectionResult> list)
        {
            unitOfWork.DetectionResultRepository.Insert(list);
            return unitOfWork.Save() > 0;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.DetectionResultRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool Delete(Expression<Func<KSC_DetectionResult, bool>> filter)
        {
            unitOfWork.DetectionResultRepository.DeleteByFilter(filter);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_DetectionResult model)
        {
            unitOfWork.DetectionResultRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_DetectionResult GetModelById(int id)
        {
            return unitOfWork.DetectionResultRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_DetectionResult GetModel(Expression<Func<KSC_DetectionResult, bool>> filter)
        {
            return unitOfWork.DetectionResultRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_DetectionResult> GetList(Expression<Func<KSC_DetectionResult, bool>> filter, bool isEdit = false)
        {
            List<KSC_DetectionResult> list = unitOfWork.DetectionResultRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_DetectionResult> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_DetectionResult, bool>> filter = null,
            Func<IQueryable<KSC_DetectionResult>, IOrderedQueryable<KSC_DetectionResult>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.DetectionResultRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}
