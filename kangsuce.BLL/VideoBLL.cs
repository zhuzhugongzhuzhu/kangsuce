﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class VideoBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_Video model)
        {
            unitOfWork.VideoRepository.Insert(model);
            unitOfWork.Save();
            return model.videoId;
        }

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool Insert(List<KSC_Video> list)
        {
            unitOfWork.VideoRepository.Insert(list);
            return unitOfWork.Save() > 0;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.VideoRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool Delete(Expression<Func<KSC_Video, bool>> filter)
        {
            unitOfWork.VideoRepository.DeleteByFilter(filter);
            return unitOfWork.Save() > 0;
        }

        public bool Delete(List<int> idList, string path)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    List<KSC_Video> list = GetList(p => idList.Contains(p.videoId));
                    foreach (KSC_Video model in list)
                    {
                        unitOfWork.VideoRepository.Delete(model.videoId);
                    }
                    unitOfWork.Save();
                    scope.Complete();
                    foreach (
                        string localPath in
                            list.Select(model => Path.Combine(path, model.filePath.Replace("/Upload/", ""))))
                    {
                        System.IO.File.Delete(localPath);
                    }
                    foreach (
                        string localPath in
                            list.Select(model => Path.Combine(path, model.imgPath.Replace("/Upload/", ""))))
                    {
                        System.IO.File.Delete(localPath);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    return false;
                }
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_Video model)
        {
            unitOfWork.VideoRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_Video GetModelById(int id)
        {
            return unitOfWork.VideoRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_Video GetModel(Expression<Func<KSC_Video, bool>> filter)
        {
            return unitOfWork.VideoRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_Video> GetList(Expression<Func<KSC_Video, bool>> filter, bool isEdit = false)
        {
            List<KSC_Video> list = unitOfWork.VideoRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_Video> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_Video, bool>> filter = null,
            Func<IQueryable<KSC_Video>, IOrderedQueryable<KSC_Video>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.VideoRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}
