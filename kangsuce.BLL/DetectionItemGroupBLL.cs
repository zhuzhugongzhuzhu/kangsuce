﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class DetectionItemGroupBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_DetectionItemGroup model)
        {
            unitOfWork.DetectionItemGroupRepository.Insert(model);
            unitOfWork.Save();
            return model.detectionItemGroupId;
        }

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool Insert(List<KSC_DetectionItemGroup> list)
        {
            unitOfWork.DetectionItemGroupRepository.Insert(list);
            return unitOfWork.Save() > 0;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.DetectionItemGroupRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool Delete(Expression<Func<KSC_DetectionItemGroup, bool>> filter)
        {
            unitOfWork.DetectionItemGroupRepository.DeleteByFilter(filter);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_DetectionItemGroup model)
        {
            unitOfWork.DetectionItemGroupRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_DetectionItemGroup GetModelById(int id)
        {
            return unitOfWork.DetectionItemGroupRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_DetectionItemGroup GetModel(Expression<Func<KSC_DetectionItemGroup, bool>> filter)
        {
            return unitOfWork.DetectionItemGroupRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_DetectionItemGroup> GetList(Expression<Func<KSC_DetectionItemGroup, bool>> filter, bool isEdit = false)
        {
            List<KSC_DetectionItemGroup> list = unitOfWork.DetectionItemGroupRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_DetectionItemGroup> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_DetectionItemGroup, bool>> filter = null,
            Func<IQueryable<KSC_DetectionItemGroup>, IOrderedQueryable<KSC_DetectionItemGroup>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.DetectionItemGroupRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}
