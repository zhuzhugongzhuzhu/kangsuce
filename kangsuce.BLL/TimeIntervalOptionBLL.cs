﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class TimeIntervalOptionBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_TimeIntervalOption model)
        {
            unitOfWork.TimeIntervalOptionRepository.Insert(model);
            unitOfWork.Save();
            return model.timeIntervalId;
        }

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool Insert(List<KSC_TimeIntervalOption> list)
        {
            unitOfWork.TimeIntervalOptionRepository.Insert(list);
            return unitOfWork.Save() > 0;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.TimeIntervalOptionRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool Delete(Expression<Func<KSC_TimeIntervalOption, bool>> filter)
        {
            unitOfWork.TimeIntervalOptionRepository.DeleteByFilter(filter);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_TimeIntervalOption model)
        {
            unitOfWork.TimeIntervalOptionRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_TimeIntervalOption GetModelById(int id)
        {
            return unitOfWork.TimeIntervalOptionRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_TimeIntervalOption GetModel(Expression<Func<KSC_TimeIntervalOption, bool>> filter)
        {
            return unitOfWork.TimeIntervalOptionRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_TimeIntervalOption> GetList(Expression<Func<KSC_TimeIntervalOption, bool>> filter, bool isEdit = false)
        {
            List<KSC_TimeIntervalOption> list = unitOfWork.TimeIntervalOptionRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_TimeIntervalOption> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_TimeIntervalOption, bool>> filter = null,
            Func<IQueryable<KSC_TimeIntervalOption>, IOrderedQueryable<KSC_TimeIntervalOption>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.TimeIntervalOptionRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}
