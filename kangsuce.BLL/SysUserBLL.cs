﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class SysUserBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_SysUser model)
        {
            unitOfWork.SysUserRepository.Insert(model);
            unitOfWork.Save();
            return model.userId;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.SysUserRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_SysUser model)
        {
            unitOfWork.SysUserRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_SysUser GetModelById(int id)
        {
            return unitOfWork.SysUserRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="isShowRole"></param>
        /// <returns></returns>
        public KSC_SysUser GetModel(Expression<Func<KSC_SysUser, bool>> filter, bool isShowRole = false)
        {
            KSC_SysUser model = unitOfWork.SysUserRepository.GetModel(filter);
            if (model == null) return null;
            if (!isShowRole) return model;
            model.userRoleList = unitOfWork.UserRoleRepository.GetList(p => p.userId == model.userId);
            foreach (KSC_UserRole userRole in model.userRoleList)
            {
                userRole.rolePermissionList =
                    unitOfWork.RolePermissionRepository.GetList(p => p.roleId == userRole.roleId);
            }

            return model;
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_SysUser> GetList(Expression<Func<KSC_SysUser, bool>> filter, bool isEdit = false)
        {
            List<KSC_SysUser> list = unitOfWork.SysUserRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 查询实体列表（包括子集集合）
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_SysUser> GetListForEach(Expression<Func<KSC_SysUser, bool>> filter, bool isEdit = false)
        {
            List<KSC_SysUser> list = unitOfWork.SysUserRepository.GetList(filter, isEdit);
            foreach (KSC_SysUser sysUser in list)
            {
                sysUser.userRoleList = unitOfWork.UserRoleRepository.GetList(p => p.userId == sysUser.userId);
                foreach (KSC_UserRole userRole in sysUser.userRoleList)
                {
                    userRole.rolePermissionList =
                        unitOfWork.RolePermissionRepository.GetList(p => p.roleId == userRole.roleId);
                }
            }
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_SysUser> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_SysUser, bool>> filter = null,
            Func<IQueryable<KSC_SysUser>, IOrderedQueryable<KSC_SysUser>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.SysUserRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}