﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class TimeIntervalBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_TimeInterval model)
        {
            unitOfWork.TimeIntervalRepository.Insert(model);
            unitOfWork.Save();
            return model.timeIntervalId;
        }

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool Insert(List<KSC_TimeInterval> list)
        {
            unitOfWork.TimeIntervalRepository.Insert(list);
            return unitOfWork.Save() > 0;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.TimeIntervalRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool Delete(Expression<Func<KSC_TimeInterval, bool>> filter)
        {
            unitOfWork.TimeIntervalRepository.DeleteByFilter(filter);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_TimeInterval model)
        {
            unitOfWork.TimeIntervalRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_TimeInterval GetModelById(int id)
        {
            return unitOfWork.TimeIntervalRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_TimeInterval GetModel(Expression<Func<KSC_TimeInterval, bool>> filter)
        {
            return unitOfWork.TimeIntervalRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_TimeInterval> GetList(Expression<Func<KSC_TimeInterval, bool>> filter, bool isEdit = false)
        {
            List<KSC_TimeInterval> list = unitOfWork.TimeIntervalRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_TimeInterval> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_TimeInterval, bool>> filter = null,
            Func<IQueryable<KSC_TimeInterval>, IOrderedQueryable<KSC_TimeInterval>> orderBy = null, bool isEdit = false)
        {
            return unitOfWork.TimeIntervalRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy, isEdit);
        }
    }
}
