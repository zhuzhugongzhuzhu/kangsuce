﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using kangsuce.DAL;
using kangsuce.Model;

namespace kangsuce.BLL
{
    public class RolePermissionBLL
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(KSC_RolePermission model)
        {
            unitOfWork.RolePermissionRepository.Insert(model);
            unitOfWork.Save();
            return model.rolePermissionId;
        }

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool Insert(List<KSC_RolePermission> list)
        {
            unitOfWork.RolePermissionRepository.Insert(list);
            return unitOfWork.Save() > 0;
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            unitOfWork.RolePermissionRepository.Delete(id);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据条件批量删除实体
        /// </summary>
        /// <returns></returns>
        public bool DeleteByFilter(Expression<Func<KSC_RolePermission, bool>> filter)
        {
            unitOfWork.RolePermissionRepository.DeleteByFilter(filter);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 删除所有实体
        /// </summary>
        /// <returns></returns>
        public bool DeleteAll()
        {
            unitOfWork.RolePermissionRepository.DeleteAll();
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(KSC_RolePermission model)
        {
            unitOfWork.RolePermissionRepository.Update(model);
            return unitOfWork.Save() > 0;
        }

        /// <summary>
        /// 根据Id获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KSC_RolePermission GetModelById(int id)
        {
            return unitOfWork.RolePermissionRepository.GetModelByID(id);
        }

        /// <summary>
        /// 根据条件获取实体
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public KSC_RolePermission GetModel(Expression<Func<KSC_RolePermission, bool>> filter)
        {
            return unitOfWork.RolePermissionRepository.GetModel(filter);
        }

        /// <summary>
        /// 查询实体列表
        /// </summary>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_RolePermission> GetList(Expression<Func<KSC_RolePermission, bool>> filter, bool isEdit = false)
        {
            List<KSC_RolePermission> list = unitOfWork.RolePermissionRepository.GetList(filter, isEdit);
            return list;
        }

        /// <summary>
        /// 分页获取实体列表
        /// </summary>
        /// <param name="pageSize">页面大小</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="total">返回总数</param>
        /// <param name="filter">查询条件lamda表达式  实例：c => true</param>
        /// <param name="orderBy">排序 实例：c=>c.OrderBy(p=>p.Name)</param>
        /// <param name="isEdit">是否用于编辑</param>
        /// <returns></returns>
        public List<KSC_RolePermission> GetListByPage(int pageSize, int pageIndex, out int total,
            Expression<Func<KSC_RolePermission, bool>> filter = null,
            Func<IQueryable<KSC_RolePermission>, IOrderedQueryable<KSC_RolePermission>> orderBy = null,
            bool isEdit = false)
        {
            return unitOfWork.RolePermissionRepository.GetListByPage(pageSize, pageIndex, out total, filter, orderBy,
                isEdit);
        }
    }
}