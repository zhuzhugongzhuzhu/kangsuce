﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using kangsuce.BLL;
using kangsuce.Model;

namespace kangsuce.Admin.Controllers
{
    public class HomeController : BaseController
    {
        #region BLL

        private readonly SysUserBLL sysUserBLL = new SysUserBLL();
        private readonly FunctionBLL functionBLL = new FunctionBLL();

        #endregion

        // GET: Home
        public ActionResult Index()
        {
            List<KSC_Function> list = functionBLL.GetList(p => p.functionDisplay == "1");
            if (userInfo.userType != 1)
            {
                //不是超级管理员筛选有权限的菜单
                List<KSC_UserRole> urList = userInfo.userRoleList;
                List<int> menuIds = (from item in urList from item1 in item.rolePermissionList select item1.menuId).Distinct().ToList();
                foreach (KSC_Function item in list)
                {
                    for (int i = 0; i < item.menuList.Count; i++)
                    {
                        if (menuIds.Contains(item.menuList[i].menuId)) continue;
                        item.menuList.RemoveAt(i);
                        i--;
                    }
                }
            }
            ViewBag.List = list;
            ViewBag.userName = userInfo.userName;
            ViewBag.userId = userInfo.userId;
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoginValidate(string loginName, string passWord)
        {
            try
            {
                KSC_SysUser model = sysUserBLL.GetModel(p => p.loginName == loginName && p.passWord == passWord,true);
                if (model == null)
                {
                    return Json(new { result = "error", msg = "用户名或密码错误" });
                }
                if (model.isDisable == "1")
                {
                    return Json(new { result = "error", msg = "该管理员帐号已禁用，不能登录" });
                }
                model.lastLoginDate = DateTime.Now;
                sysUserBLL.Update(model);
                Session["user"] = model;
                HttpCookie cookie = new HttpCookie("userId");
                cookie.Expires = DateTime.Now.AddDays(1);
                cookie.Value = model.userId.ToString();
                Response.Cookies.Add(cookie);
                return Json(new {result = "success", data = "登录成功"});
            }
            catch (Exception ex)
            {
                return Json(new {result = "error", msg = ex.Message});
            }
        }

        /// <summary>
        /// 桌面首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Desktop()
        {
            return View();
        }
    }
}