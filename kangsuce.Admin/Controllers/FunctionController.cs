﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace kangsuce.Admin.Controllers
{
    public class FunctionController : BaseController
    {
        private readonly BLL.FunctionBLL functionBLL = new BLL.FunctionBLL();

        /// <summary>
        /// 获取菜单数据
        /// </summary>
        /// <returns></returns>
        public ActionResult GetFunctionList()
        {
            try
            {
                var list = functionBLL.GetList(p => p.functionDisplay == "1");
                return Json(new { result = "success", data = list }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = "error", msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}