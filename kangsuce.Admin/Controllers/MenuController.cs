﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using kangsuce.Model;
using kangsuce.Common;
using Newtonsoft.Json;

namespace kangsuce.Admin.Controllers
{
    public class MenuController : BaseController
    {
        private BLL.FunctionBLL functionBLL = new BLL.FunctionBLL();
        private BLL.RolePermissionBLL rolePermissionBLL = new BLL.RolePermissionBLL();
        private BLL.MenuBLL menuBLL = new BLL.MenuBLL();
        // GET: Menu
        public ActionResult MenuRoleSet()
        {
            ViewBag.List = functionBLL.GetList(p => p.functionDisplay == "1", "");
            return View();
        }

        public ActionResult GetMenuRoleList(int roleId)
        {
            try
            {
                var list = functionBLL.GetMenuList(p => true, roleId);
                return Json(new {result = "success", data = list}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {result = "error", msg = ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 权限设置保存
        /// </summary>
        /// <param name="ids">菜单id集合</param>
        /// <param name="roleId">权限id</param>
        /// <returns></returns>
        public ActionResult MenuRoleSave(string ids, int roleId = 0)
        {
            try
            {
                rolePermissionBLL.DeleteByFilter(p => p.roleId == roleId);
                if (ids != "")
                {
                    List<KSC_RolePermission> list = new List<KSC_RolePermission>();
                    foreach (string item in ids.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (item.Split(',').Length == 2)
                        {
                            int fId = Convert.ToInt32(item.Split(',')[0]);
                            int mId = Convert.ToInt32(item.Split(',')[1]);
                            KSC_RolePermission model = new KSC_RolePermission
                            {
                                functionId = fId,
                                roleId = roleId,
                                menuId = mId
                            };
                            list.Add(model);
                        }
                    }
                    bool result = rolePermissionBLL.Insert(list);
                    return Json(new {result = result ? "success" : "error", msg = result ? "保存成功" : "保存失败"});
                }
                return Json(new {result = "success"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {result = "error", msg = ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 菜单列表
        /// </summary>
        /// <returns></returns>
        public ActionResult MenuList()
        {
            return View();
        }

        /// <summary>
        /// 获取菜单列表数据
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sqlWhere">条件</param>
        /// <returns></returns>
        public ActionResult GetMenuList(int pageSize, int pageNumber, string sqlWhere = "1=1")
        {
            try
            {
                int total;

                string where = " 1=1 ";
                if (sqlWhere != "")
                {
                    where =
                        $" f.functionName like '%{sqlWhere}%' or m.menuName like '%{sqlWhere}%' or m.menuUrl like '%{sqlWhere}%'";
                }
                var list = menuBLL.GetListForFunction(pageSize, pageNumber, out total, where);
                return Json(new {rows = list, total = total}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {result = "error", msg = ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 菜单编辑
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult MenuEdit(int id = 0)
        {
            KSC_Menu model = menuBLL.GetModelById(id) ?? new KSC_Menu {menuId = 0};
            ViewBag.Json = JsonConvert.SerializeObject(model);
            return View();
        }


        public ActionResult MenuSave(KSC_Menu model)
        {
            try
            {
                int videoId = Request.Form["menuId"].ToInt32();
                if (videoId == 0)
                {
                    model.parentId = 0;
                    model.menuDisplay = "1";
                    videoId = menuBLL.Insert(model);
                    return Json(new {result = videoId > 0 ? "success" : "error", msg = videoId > 0 ? "新增成功" : "新增失败"});
                }
                else
                {
                    KSC_Menu editModel = menuBLL.GetModelById(videoId);
                    editModel.functionId = model.functionId; //模块id
                    editModel.menuIcon = model.menuIcon; //菜单图标
                    editModel.menuName = model.menuName; //菜单名称
                    editModel.menuOrder = model.menuOrder; //排序
                    editModel.menuUrl = model.menuUrl; //菜单url
                    bool result = menuBLL.Update(editModel);
                    return Json(new {result = result ? "success" : "error", msg = result ? "修改成功" : "修改失败"});
                }
            }
            catch (Exception ex)
            {
                return Json(new {result = "error", msg = ex.Message});
            }
        }


        /// <summary>
        /// 显示/隐藏菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult MenuChangeDisplay(int id = 0)
        {
            try
            {
                if (id == 0)
                {
                    return Json(new {result = "error", msg = "传入参数有误"}, JsonRequestBehavior.AllowGet);
                }
                KSC_Menu model = menuBLL.GetModelById(id);
                model.menuDisplay = model.menuDisplay == "1" ? "0" : "1";
                bool result = menuBLL.Update(model);
                return Json(new {result = result ? "success" : "error", msg = result ? "操作成功" : "操作失败"},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {result = "error", msg = ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }
    }
}