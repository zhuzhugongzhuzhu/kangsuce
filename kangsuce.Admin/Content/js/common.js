﻿var model;
$(function() {
    if (model) {
        $.each(model, function(i, n) {
            var $element = $("[name='" + i + "']");
            if ($element.length == 0) {
                return;
            }
            var tagName = $element.get(0).tagName.toLowerCase();
            switch (tagName) {
            case "input":
                switch ($element.attr("type").toLowerCase()) {
                case "text":
                    $element.val(n);
                    break;
                case "radio":
                    $.each($element, function(index, item) {
                        if ($(item).val() == n) {
                            $(item).attr("checked", true);
                        }
                    });
                    break;
                case "chexkbox":
                    $.each($element, function(index, item) {
                        if ($(item).val() == n) {
                            $(item).attr("checked", true);
                        }
                    });
                    break;
                default:
                }
                break;
            case "select":
                $element.val(n);
                break;
            case "img":
                $element.attr("src", n);
                if ($element.attr("data-imgSrc") != undefined) {
                    $element.attr("data-imgSrc", n);
                }
                break;
            default:
            }
        });
    }
});

function getCookies(bigname, smallname) {
    var re = new RegExp("(\;|^)[^;]*(" + bigname + ")\=([^;]*)(;|$)");
    var match = re.exec(document.cookie);
    if (match) {
        var cookieValue = match != null ? match[3] : null;
        var reg = new RegExp("(^|&*)" + smallname + "=([^&]*)(&|$)");
        var r = cookieValue.match(reg);
        if (r != null) return r[2];
    }
    return null;
}

function getCookie(name) {
    var strCookie = document.cookie;
    var arrCookie = strCookie.split("; ");
    for (var i = 0; i < arrCookie.length; i++) {
        var arr = arrCookie[i].split("=");
        if (arr[0] == name) return arr[1];
    }
    return null;
}


function delCookie(name) {
    var cval = getCookie(name);
    if (cval != null) {
        var date = new Date();
        date.setTime(date.getTime() - 1);
        document.cookie = name + "=v; expires=" + date.toGMTString();
    }
}


function addTab(href, title) {
    top.window.addTab(href, title);
}

var Common = {
    /**
     * 格式化日期（不含时间）yyy-MM-dd
     */
    formatterDate: function(date) {
        if (date == undefined || date == "") {
            return "";
        }
        /*json格式时间转js时间格式*/
        date = date.substr(1, date.length - 2);
        var obj = eval('(' + "{Date: new " + date + "}" + ')');
        date = obj["Date"];
        if (date.getFullYear() < 1900) {
            return "";
        }

        var datetime = date.getFullYear()
            + "-" // "年"
            + ((date.getMonth() + 1) > 10 ? (date.getMonth() + 1) : "0"
                + (date.getMonth() + 1))
            + "-" // "月"
            + (date.getDate() < 10 ? "0" + date.getDate() : date
                .getDate());
        return datetime;
    },
    /**
     * 格式化日期（含时间"00:00:00"） yyy-MM-dd HH:mm:ss
     */
    formatterDate2: function(date) {
        if (date == undefined) {
            return "";
        }
        /*json格式时间转js时间格式*/
        date = date.substr(1, date.length - 2);
        var obj = eval('(' + "{Date: new " + date + "}" + ')');
        date = obj["Date"];
        if (date.getFullYear() < 1900) {
            return "";
        }

        /*把日期格式化*/
        var datetime = date.getFullYear()
            + "-" // "年"
            + ((date.getMonth() + 1) > 10 ? (date.getMonth() + 1) : "0"
                + (date.getMonth() + 1))
            + "-" // "月"
            + (date.getDate() < 10 ? "0" + date.getDate() : date
                .getDate()) + " " + "00:00:00";
        return datetime;
    },
    /**
     * 格式化去日期（含时间）
     */
    formatterDateTime: function(date) {
        if (date == undefined) {
            return "";
        }
        /*json格式时间转js时间格式*/
        date = date.substr(1, date.length - 2);
        var obj = eval('(' + "{Date: new " + date + "}" + ')');
        date = obj["Date"];
        if (date.getFullYear() < 1900) {
            return "";
        }

        var datetime = date.getFullYear()
            + "-" // "年"
            + ((date.getMonth() + 1) > 10 ? (date.getMonth() + 1) : "0"
                + (date.getMonth() + 1))
            + "-" // "月"
            + (date.getDate() < 10 ? "0" + date.getDate() : date
                .getDate())
            + " "
            + (date.getHours() < 10 ? "0" + date.getHours() : date
                .getHours())
            + ":"
            + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date
                .getMinutes())
            + ":"
            + (date.getSeconds() < 10 ? "0" + date.getSeconds() : date
                .getSeconds());
        return datetime;
    },
    dataInit: function() {
        if (model) {
            $.each(model, function (i, n) {
                var $element = $("[name='" + i + "']");
                if ($element.length == 0) {
                    return;
                }
                var tagName = $element.get(0).tagName.toLowerCase();
                switch (tagName) {
                    case "input":
                        switch ($element.attr("type").toLowerCase()) {
                            case "text":
                                $element.val(n);
                                break;
                            case "radio":
                                $.each($element, function (index, item) {
                                    if ($(item).val() == n) {
                                        $(item).attr("checked", true);
                                    }
                                });
                                break;
                            case "chexkbox":
                                $.each($element, function (index, item) {
                                    if ($(item).val() == n) {
                                        $(item).attr("checked", true);
                                    }
                                });
                                break;
                            default:
                        }
                        break;
                    case "select":
                        $element.val(n);
                        break;
                    case "img":
                        $element.attr("src", n);
                        if ($element.attr("data-imgSrc") != undefined) {
                            $element.attr("data-imgSrc", n);
                        }
                        break;
                    default:
                }
            });
        }
    }
};
//获取url参数
(function($) {
    $.getUrlParam = function(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }
})(jQuery);